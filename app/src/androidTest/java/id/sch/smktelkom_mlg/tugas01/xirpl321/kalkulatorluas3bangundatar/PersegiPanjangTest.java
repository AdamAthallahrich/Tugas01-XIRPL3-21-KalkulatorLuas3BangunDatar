package id.sch.smktelkom_mlg.tugas01.xirpl321.kalkulatorluas3bangundatar;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Adam_A on 25/01/2018.
 */

@RunWith(AndroidJUnit4.class)
public class PersegiPanjangTest {
    @Rule
    public ActivityTestRule<HitungPersegiPanjang> mActivityTestRule
            = new ActivityTestRule<>(HitungPersegiPanjang.class);

    @Test
    public void clickBtnHitung() {
        Espresso.onView(ViewMatchers.withId(R.id.btnHitung))
                .perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.txtLuas))
                .check(ViewAssertions.matches(ViewMatchers.withText("50")));
    }
}
