package id.sch.smktelkom_mlg.tugas01.xirpl321.kalkulatorluas3bangundatar;

/**
 * Created by Adam_A on 20/01/2018.
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HitungLingkaran extends Activity {
    private EditText txtJari;
    private EditText txtLuas;
    private Button btnHitung;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.lingkaran);
        txtJari = findViewById(R.id.txtJari);
        txtLuas = findViewById(R.id.txtLuas);
        btnHitung = findViewById(R.id.btnHitung);

    }

    public void hitungLuas(View view) {
        try {
            int jarijari = Integer.parseInt(txtJari.getText().toString());
            double phi = 3.14;
            double luas = phi * jarijari * jarijari;
            txtLuas.setText(String.valueOf(luas));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backtoMenu(View view) {
        finish();
    }
}

