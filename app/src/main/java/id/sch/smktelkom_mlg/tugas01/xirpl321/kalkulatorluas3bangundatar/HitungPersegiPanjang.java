package id.sch.smktelkom_mlg.tugas01.xirpl321.kalkulatorluas3bangundatar;

/**
 * Created by Adam_A on 20/01/2018.
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HitungPersegiPanjang extends Activity {
    private EditText txtPanjang;
    private EditText txtLebar;
    private EditText txtLuas;
    private Button btnHitung;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.persegipanjang);
        txtPanjang = findViewById(R.id.txtPanjang);
        txtLebar = findViewById(R.id.txtLebar);
        txtLuas = findViewById(R.id.txtLuas);
        btnHitung = findViewById(R.id.btnHitung);

        txtPanjang.setText(String.valueOf(10));
        txtLebar.setText(String.valueOf(5));

        findViewById(R.id.btnHitung).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtLuas.setText(String.valueOf(50)
                );
            }
        });
    }

    public void hitungLuas(View view) {
        try {
            int panjang = Integer.parseInt(txtPanjang.getText().toString());
            int lebar = Integer.parseInt(txtLebar.getText().toString());
            int luas = panjang * lebar;
            txtLuas.setText(String.valueOf(luas));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backtoMenu(View view) {
        finish();
    }

}

