package id.sch.smktelkom_mlg.tugas01.xirpl321.kalkulatorluas3bangundatar;

/**
 * Created by Adam_A on 20/01/2018.
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HitungSegitiga extends Activity {
    private EditText txtAlas;
    private EditText txtTinggi;
    private EditText txtLuas;
    private Button btnHitung;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.segitiga);
        txtAlas = findViewById(R.id.txtAlas);
        txtTinggi = findViewById(R.id.txtTinggi);
        txtLuas = findViewById(R.id.txtLuas);
        btnHitung = findViewById(R.id.btnHitung);

        txtAlas.setText(String.valueOf(20));
        txtTinggi.setText(String.valueOf(30));

        findViewById(R.id.btnHitung).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtLuas.setText(String.valueOf(300));
            }
        });

    }

    public void hitungLuas(View view) {
        try {
            int alas = Integer.parseInt(txtAlas.getText().toString());
            int tinggi = Integer.parseInt(txtTinggi.getText().toString());
            int luas = (alas * tinggi) / 2;
            txtLuas.setText(String.valueOf(luas));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backtoMenu(View view) {
        finish();
    }
}
